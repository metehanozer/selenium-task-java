# 🎁 Selenium Task Java

Java TestNG ile selenium test projesi...

### Ön Koşullar

TestData.properties dosyasına amazon.com.tr için geçerli email ve password bilgileri girilmelidir.\
ChromeDriver path'e eklenmelidir. Ya da BaseTest.java dosyası System.setProperty alanında driver yolu belirtilmelidir.

### Test

Testi çalıştırmak için...
~~~
.\mvnw clean test
~~~
