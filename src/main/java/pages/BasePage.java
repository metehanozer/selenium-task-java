package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePage {

    private static final int TIMEOUT = 10;
    private static final int POLLING = 100;

    public WebDriver webDriver;
    public String selectedProduct;  // Seçilen ürünü doğrulamak için bu değişken kullanılıyor.
    private WebDriverWait webDriverWait;

    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriverWait = new WebDriverWait(webDriver, TIMEOUT, POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(webDriver, TIMEOUT), this);
    }

    public BasePage(WebDriver webDriver, String selectedProduct) {
        this.webDriver = webDriver;
        this.selectedProduct = selectedProduct;
        webDriverWait = new WebDriverWait(webDriver, TIMEOUT, POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(webDriver, TIMEOUT), this);
    }

    public void refreshPage() {
        webDriver.navigate().refresh();
    }

    public String getTextTo(By by) {
        return waitForFindVisible(by).getText();
    }

    public String getAttributeTo(By by, String attribute) {
        return waitForFindVisible(by).getAttribute(attribute);
    }

    public void sendKeysTo(By by, String string) {
        waitForFindVisible(by).sendKeys(string);
    }

    public void clickTo(By by) {
        waitForFindClick(by).click();
    }

    public void hoverTo(By by) {
        WebElement webElement = webDriver.findElement(by);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(webElement).perform();
    }

    public void runScriptTo(By by) {
        WebElement webElement = waitForFindClick(by);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)webDriver;
        javascriptExecutor.executeScript("arguments[0].click();", webElement);
    }

    public List<WebElement> findElements(By by) {
        waitForFindVisible(by);
        return webDriver.findElements(by);
    }

    private WebElement waitForFindVisible(By by) {
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return webDriver.findElement(by);
    }

    private WebElement waitForFindClick(By by) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(by));
        return webDriver.findElement(by);
    }

    public static void scrollToBottom(WebDriver driver) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

}
