package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.DataManager;

public class HomePage extends BasePage {

    public HomePage(WebDriver webDriver){
        super(webDriver);
    }

    String baseURL = DataManager.getInstance().getBaseUrl();

    By cssNavlogoLink = By.cssSelector("div#nav-logo > .nav-logo-link");
    By logoutAccountListA = By.cssSelector("a#nav-link-accountList");
    By cssSignin = By.cssSelector("div#nav-flyout-ya-signin  .nav-action-inner");
    By loginAccountListA = By.cssSelector("a#nav-link-accountList > .nav-line-1");
    By searchInput = By.id("twotabsearchtextbox");
    By searchButton = By.cssSelector("[value='Git']");

    public HomePage goToHomePage() {
        webDriver.get(baseURL);
        return this;
    }

    public HomePage verifyHomePage(String expectedText) {
        Assert.assertEquals(getAttributeTo(cssNavlogoLink, "aria-label"), expectedText);
        return this;
    }

    public HomePage hoverAccountList() {
        hoverTo(logoutAccountListA);
        return this;
    }

    public LoginPage clickSignin() {
        clickTo(cssSignin);
        return new LoginPage(webDriver);
    }

    public void verifyLoginUsername(String expectedText) {
        Assert.assertEquals(getTextTo(loginAccountListA), expectedText);
    }

    public SearchResultPage searchKeyword(String keyword) {
        sendKeysTo(searchInput, keyword);
        clickTo(searchButton);
        return new SearchResultPage(webDriver);
    }

}
