package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver webDriver){
        super(webDriver);
    }

    By emailInput = By.id("ap_email");
    By continueButton = By.cssSelector("input#continue");
    By passwordInput = By.id("ap_password");
    By signinButton = By.cssSelector("input#signInSubmit");

    public LoginPage sendEmail(String email) {
        sendKeysTo(emailInput, email);
        clickTo(continueButton);
        return this;
    }

    public HomePage sendPassword(String password) {
        sendKeysTo(passwordInput, password);
        clickTo(signinButton);
        return new HomePage(webDriver);
    }

}
