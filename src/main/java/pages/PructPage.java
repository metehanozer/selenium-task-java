package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PructPage extends BasePage {

    public PructPage(WebDriver webDriver, String selectedProduct){
        super(webDriver, selectedProduct);
    }

    By addTolistButton = By.cssSelector("input#add-to-wishlist-button-submit");
    By showListButton = By.cssSelector("a#WLHUC_viewlist  .w-button-text");
    By productTitle = By.cssSelector("span#productTitle");

    // ürün wish liste ekleniyor. Daha sonra doğrulama için selectedProductName değişkeni kullanılacak.
    public WishListPage addProductToList() {
        selectedProduct = getTextTo(productTitle);
        clickTo(addTolistButton);
        clickTo(showListButton);
        return new WishListPage(webDriver, selectedProduct);
    }

}
