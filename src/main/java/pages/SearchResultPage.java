package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class SearchResultPage extends BasePage {

    public SearchResultPage(WebDriver webDriver){
        super(webDriver);
    }

    By searhedKeyword = By.cssSelector(".a-color-state.a-text-bold");
    By nextPageButton = By.cssSelector(".a-last > a");
    By selectedPage = By.cssSelector(".a-selected > a");
    By productListSpan = By.xpath("//div[@class='s-result-list s-search-results sg-row']/div/div/span/div/div/div[2]/div/div/div/span/a");

    public SearchResultPage verifySearhedKeyword(String expectedText) {
        Assert.assertEquals(getTextTo(searhedKeyword), expectedText);
        return this;
    }

    public SearchResultPage gotToNextPage() {
        clickTo(nextPageButton);
        return this;
    }

    public SearchResultPage verifySelectedPage(String expectedText) {
        Assert.assertEquals(getTextTo(selectedPage), expectedText);
        return this;
    }

    public PructPage getProductByIndex(int index) {
        List<WebElement> productList = findElements(productListSpan);
        productList.get(index).click();
        return new PructPage(webDriver, selectedProduct);
    }

}
