package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;

public class WishListPage extends BasePage {

    public WishListPage(WebDriver webDriver, String selectedProduct){
        super(webDriver, selectedProduct);
    }

    By wishList = By.xpath("//div[@class='a-row a-size-small']/h3/a");
    By removeItemLink = By.linkText("Ürünü kaldır");
    By emptylistMessage = By.cssSelector(".a-color-base.a-size-medium");

    // wish liste en son eklenen ürün doğrulanıyor.
    public WishListPage verifyWishListFirstItem() {
        Assert.assertEquals(getAttributeTo(wishList, "title"), selectedProduct);
        return this;
    }

    // Daha önceden eklenmiş ürünler olabilir diye bütün ürünler siliniyor.
    public WishListPage removeItem() {
        while (true) {
            try {
                clickTo(removeItemLink);
                System.out.println("--- wish list dolu, ilk ürün silliniyor ---");
                refreshPage();
            } catch (WebDriverException ex) {
                System.out.println("--- wish list boş ---");
                break;
            }
        }
        return this;
    }

    // Wish litin boş olduğu doğrulamıyor.
    public void verifyRemovedItem(String expectedText) {
        Assert.assertEquals(getTextTo(emptylistMessage), expectedText);
    }

}
