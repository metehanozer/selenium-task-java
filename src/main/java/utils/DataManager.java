package utils;

import java.io.IOException;
import java.util.Properties;

public class DataManager {

    private static DataManager instance;
    private static final Object lock = new Object();
    private static String baseUrl;
    private static String email;
    private static String password;
    private static String keyword;

    public static DataManager getInstance () {
        if (instance == null) {
            synchronized (lock) {
                instance = new DataManager();
                instance.loadData();
            }
        }
        return instance;
    }

    private void loadData() {
        Properties properties = new Properties();

        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("TestData.properties"));
        } catch (IOException e) {
            System.out.println("TestData properties file cannot be found");
        }

        baseUrl = properties.getProperty("base_url");
        email = properties.getProperty("email");
        password = properties.getProperty("password");
        keyword = properties.getProperty("keyword");
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getKeyword() {
        return keyword;
    }

}
