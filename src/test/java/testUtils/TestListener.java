package testUtils;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println("--- " + iTestResult.getName() + " Test Pass ---");
        super.onTestSuccess(iTestResult);
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("--- " + iTestResult.getName() + " Test Fail ---");
        super.onTestFailure(iTestResult);
    }
}
