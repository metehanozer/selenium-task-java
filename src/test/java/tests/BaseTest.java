package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.*;
import pages.HomePage;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected WebDriver webDriver;
    public HomePage homePage;

    public WebDriver getWebDriver() {
        return webDriver;
    }

    @BeforeClass(alwaysRun=true)
    public void setUpDriver() {
        System.out.println("--- Google Chrome Driver açılıyor ---");

        ChromeOptions chromeOptions = new ChromeOptions();
        //chromeOptions.addArguments("disable-infobars");  // WONT WORK TODO
        //chromeOptions.addArguments("--headless");
        //chromeOptions.addArguments("--incognito");
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("disable-geolocation");

        //System.setProperty("webdriver.chrome.driver", "C:/drivers/chromedriver.exe");
        System.setProperty("webdriver.chrome.silentOutput", "true");
        webDriver = new EventFiringWebDriver(new ChromeDriver(chromeOptions));
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //FirefoxOptions options = new FirefoxOptions();
        //webDriver = new FirefoxDriver(options);
        //webDriver.manage().window().maximize();
    }

    @BeforeMethod(alwaysRun=true)
    public void methodLevelSetup() {
        homePage = new HomePage(webDriver);
    }

    @AfterClass(alwaysRun=true)
    public void tearDownDriver() {
        System.out.println("--- Google Chrome Driver kapatılıyor ---");
        webDriver.quit();
    }

}
