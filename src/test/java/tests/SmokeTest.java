package tests;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import testUtils.TestListener;
import utils.DataManager;

@Listeners({TestListener.class})
public class SmokeTest extends BaseTest {

    String email = DataManager.getInstance().getEmail();
    String password = DataManager.getInstance().getPassword();
    String keyword = DataManager.getInstance().getKeyword();

    @Test
    public void CheckSeleniumTask() {
        homePage
                // 1-  http://www.amazon.com sitesine gelecek ve anasayfanin acildigini onaylayacak,
                .goToHomePage()
                .verifyHomePage("Amazon.de")

                // 2- Login ekranini acip, bir kullanici ile login olacak
                .hoverAccountList()
                .clickSignin()
                .sendEmail(email)
                .sendPassword(password)

                // 3- Ekranin ustundeki Search alanina 'samsung' yazip Ara butonuna tiklayacak,
                .searchKeyword(keyword)

                // 4- Gelen sayfada samsung icin sonuc bulundugunu onaylayacak,
                .verifySearhedKeyword('"' + keyword + '"')

                // 5- Arama sonuclarindan 2. sayfaya tiklayacak ve acilan sayfada 2. sayfanin su an gosterimde oldugunu onaylayacak,
                .gotToNextPage()
                .verifySelectedPage("2")

                // 6- Ustten 3. urunun icindeki 'Add to List' butonuna tiklayacak,
                .getProductByIndex(2)

                // 7- Ekranin en ustundeki 'List' linkine tiklayacak içerisinden Wish listi seçecek,
                .addProductToList()

                // 8- Acilan sayfada bir onceki sayfada izlemeye alinmis urunun bulundugunu onaylayacak,
                .verifyWishListFirstItem()

                // 9- Favorilere alinan bu urunun yanindaki 'Delete' butonuna basarak, favorilerimden cikaracak,
                .removeItem()

                // 10- Sayfada bu urunun artik favorilere alinmadigini onaylayacak.
                .verifyRemovedItem("Bu listede 0 ürün var");
    }

}
